/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 26, 2014, 4:20 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    int a = 7;
    int b = 10;
    int c = a;
    cout << "a=" << a <<" " <<  "b=" << b<< endl;
    a = b; 
    b = c;
    cout << "a=" << a <<" " <<  "b=" << b << endl;
    

    return 0;
}

