/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 26, 2014, 3:44 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //declare two integer variables
int num1, num2;
cout << num1 << "" <<num2 << endl;

// Initialize my variables
num1 = num2 = 0;

// Should expect 0 0
cout << num1 << "" << num2 << endl;

//want to get tw values from the user
// Prompt the user when we want input

cout << "Enter two intergers:" << endl;

//User input
cin >> num1 >> num2;

cout << num1 << "" << num2 << endl;

// Calculate the average of two numbers
// Average is the sum of the total values of the numbers
// Divided by the quantity of numbers

// Calculate total
int total = num1 + num2;

// Calculate average with integer division
// Calculate average with double division
// Calculate average with static casting
double averageIntDivision = total / 2;
double averageDoubleDivision = total / 2.0;
double averageStaticCast =
   static_cast<double>(total) / 2;

cout << "Average with int division:" 
        << averageIntDivision << endl;

cout << "Average with double division:" 
        << averageDoubleDivision << endl;

cout << "Average with Static Cast:" 
        << averageStaticCast << endl;

    return 0;
}

