/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 24, 2014, 5:16 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    string message; 
    message ="Hello";
           
    cout << "Hello, my name is Hal!" << endl;
    cout << "What is your name?" << endl;
    cin >> message;
    cout << "Hello, ";
    cout << message;
    cout <<". I am glad to meet you." << endl;
    
    return 0;
}

