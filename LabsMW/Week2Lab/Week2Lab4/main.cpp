/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 26, 2014, 4:54 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    double meter;
    
    
    cout << "Enter a measurement in meters:" << endl;
    cin >> meter;
    cout << meter << "meters" << endl;
    
    double mile = meter / 1609.344 ; 
    double foot = 3.281 * meter;
    double inch = 12 * foot;
    cout << "mile:" << mile << endl;
    cout << "foot:" << foot << endl;
    cout << "inch:" << inch << endl;
    
    
    return 0;
}

