/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 24, 2014, 3:22 PM
 */

#include <cstdlib> //csdlib is unneeded
#include <iostream> // iostream is for input and output from console

//std is short for standard
// all my libraries come from standard c++ namespace
// also defines context of key words
using namespace std;

// In C++ whitespace is ignored

/*
 * 
 */

// All code is located within main
// there is only 1 main per program
//code is executed top to bottom, left to right
// Argc and Argv come from outside of program

int main(int argc, char** argv) {
    
    // Variable definition
    // Data type is a string
    // Variable name is message
    // Assigning "Hello World" to message
    // string message = "Hello World";

    // All statements end in a semi colon
    // Cout means console out
    // << is the stream of operator
    // "Hello World" is a string literal
    // endl makes a new line
   // cout <<"Hello World" <<  endl;
    
    string message; // Variable declaration
    message = "Hello World"; // Variable initialization
    
    //Prompt the user
    cout << "Please enter a word" << endl;
   
    
    //cin gets input from console and stored into the variable
    //message
    cin >> message;
    
    // Output the variable "message"
    
    cout << "You entered " << message << endl;
    cout << "Erona Thi" << endl;
    // IF code gets here, then it excuted correctly
    //Return flag
    return 0;
} // All code is inside main curly brackets

